#ifndef GAZEBO_ROS_TEMPLATE_HH
#define GAZEBO_ROS_TEMPLATE_HH

#include <ros/ros.h>

#include <gazebo/physics/physics.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/common/Time.hh>
#include <gazebo/common/Plugin.hh>
#include <gazebo/common/Events.hh>

#include <ros/callback_queue.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/Range.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Empty.h>
#include <std_srvs/Empty.h>
#include <drone_msgs/Navdata.h>
#include <nav_msgs/Path.h>
#include <tf/tf.h>
#include <mavros_msgs/RangeFinder.h>

#define LANDED 0
#define FLYING 1

namespace gazebo
{

   class NavdataPlugin : public ModelPlugin
   {
      
      ros::NodeHandle* node_handle_;
      ros::CallbackQueue callback_queue_;
      ros::Subscriber velocity_subscriber_;
	  ros::Subscriber imu_subscriber_;
	  ros::Subscriber sonar_subscriber_;
	  ros::Subscriber state_subscriber_;
      ros::Subscriber take_off_subscriber_;
      ros::Subscriber land_subscriber_;
      ros::Publisher m_navdataPub;
      ros::Publisher cmd_velPub;
      ros::Publisher sonarPub;
      ros::Time state_stamp;
	  math::Pose pose;
      math::Pose truth_pose;
      math::Vector3 euler, velocity, acceleration, angular_velocity;
      physics::WorldPtr world;
      physics::LinkPtr link;
      physics::ModelPtr model_;

      std::string link_name_;
      std::string namespace_;
      std::string velocity_topic_;
      std::string takeoff_topic_;
      std::string land_topic_;
      std::string reset_topic_;
      std::string navdata_topic_;

      std::string imu_topic_;
      std::string sonar_topic_;
      std::string state_topic_;

      double robot_altitude;
      common::Time last_time;
      event::ConnectionPtr updateConnection;

	  int robot_current_state;
      sensor_msgs::ImuConstPtr imu_msg;

      double trans_drift, alt_drift;


	  geometry_msgs::Twist velocity_command_;
	  // callback functions for subscribers
	  void VelocityCallback(const geometry_msgs::TwistConstPtr&);
	  void ImuCallback(const sensor_msgs::ImuConstPtr&);
      void SonarCallback(const sensor_msgs::RangeConstPtr&);
      void StateCallback(const nav_msgs::OdometryConstPtr&);
      void Reset();
      void Update();
      void takeoffCB(std_msgs::Empty msg);
      void land(std_msgs::Empty msg);
      double x_offset, y_offset, z_offset;
      float RandomFloat(float min, float max);
      bool first;


      
      /// \brief Constructor
      public: NavdataPlugin();

      /// \brief Destructor
      public: virtual ~NavdataPlugin();

      /// \brief Load the controller
      public: void Load( physics::ModelPtr _parent, sdf::ElementPtr _sdf );

      /// \brief Update the controller
      protected: virtual void UpdateChild();



   };

}

#endif
