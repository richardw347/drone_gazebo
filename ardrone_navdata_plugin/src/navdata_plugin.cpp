#include <ros/ros.h>
#include "navdata_plugin.h"


#define TRANSLATION_ERROR 0.35
#define HEIGHT_ERROR 0.001
namespace gazebo
{
// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(NavdataPlugin);

////////////////////////////////////////////////////////////////////////////////
// Constructor
NavdataPlugin::NavdataPlugin()
{
}

////////////////////////////////////////////////////////////////////////////////
// Destructor
NavdataPlugin::~NavdataPlugin()
{
}

////////////////////////////////////////////////////////////////////////////////
// Load the controller
void NavdataPlugin::Load( physics::ModelPtr _parent, sdf::ElementPtr _sdf )
{
    // Make sure the ROS node for Gazebo has already been initalized
    if (!ros::isInitialized())
    {
        ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
                         << "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
        return;
    }

	robot_current_state = LANDED;

    model_ = _parent;
    world = _parent->GetWorld();

    if (!_sdf->HasElement("robotNamespace"))
        namespace_.clear();
    else
        namespace_ = _sdf->GetElement("robotNamespace")->GetValueString();

    if (!_sdf->HasElement("navdataTopic"))
        navdata_topic_ = "ardrone/navdata";
    else
        navdata_topic_ = _sdf->GetElement("navdataTopic")->GetValueString();

    if (!_sdf->HasElement("imuTopic"))
        imu_topic_.clear();
    else
        imu_topic_ = _sdf->GetElement("imuTopic")->GetValueString();

    if (!_sdf->HasElement("state_topic"))
        state_topic_ = "ground_truth/state";
    else
        state_topic_ = _sdf->GetElement("state_topic")->GetValueString();

    if (!_sdf->HasElement("velocityDrift"))
        trans_drift = 0.0;
    else
        _sdf->GetElement("velocityDrift")->GetValue()->Get(trans_drift);
    
    if (!_sdf->HasElement("altitudeDrift"))
        alt_drift = 0.0;
    else
        _sdf->GetElement("altitudeDrift")->GetValue()->Get(alt_drift);
        
    if (!_sdf->HasElement("sonarTopic"))
        sonar_topic_.clear();
    else
        sonar_topic_ = _sdf->GetElement("sonarTopic")->GetValueString();

    link = _parent->GetLink();
    link_name_ = link->GetName();

    if (!_sdf->HasElement("bodyName"))
    {

    }
    else {
        //link_name_ = "base_link";
        //link = boost::shared_dynamic_cast<physics::Link>(world->GetEntity(link_name_));
    }

    if (!_sdf->HasElement("takeoffTopic"))
      takeoff_topic_ = "ardrone/takeoff";
    else
      takeoff_topic_ = _sdf->GetElement("takeoffTopic")->GetValueString();

    if (!_sdf->HasElement("/ardrone/land"))
      land_topic_ = "ardrone/land";
    else
      land_topic_ = _sdf->GetElement("landTopic")->GetValueString();

    node_handle_ = new ros::NodeHandle(namespace_);

    m_navdataPub = node_handle_->advertise< drone_msgs::Navdata >( navdata_topic_ , 25 );

    cmd_velPub = node_handle_->advertise<geometry_msgs::Twist>("cmd_vel", 5);
    sonarPub = node_handle_->advertise<mavros_msgs::RangeFinder>("mavros/rangefinder", 5);

    // subscribe imu
    if (!imu_topic_.empty())
    {
        ros::SubscribeOptions ops = ros::SubscribeOptions::create<sensor_msgs::Imu>(
                    imu_topic_, 1,
                    boost::bind(&NavdataPlugin::ImuCallback, this, _1),
                    ros::VoidPtr(), &callback_queue_);
        imu_subscriber_ = node_handle_->subscribe(ops);

        ROS_INFO_NAMED("quadrotor_state_controller", "Using imu information on topic %s as source of orientation and angular velocity.", imu_topic_.c_str());
    }

    // subscribe sonar senor info
    if (!sonar_topic_.empty())
    {
        ros::SubscribeOptions ops = ros::SubscribeOptions::create<sensor_msgs::Range>(
                    sonar_topic_, 1,
                    boost::bind(&NavdataPlugin::SonarCallback, this, _1),
                    ros::VoidPtr(), &callback_queue_);
        sonar_subscriber_ = node_handle_->subscribe(ops);

        ROS_INFO_NAMED("quadrotor_state_controller", "Using sonar information on topic %s as source of altitude.", sonar_topic_.c_str());
    }

    // subscribe state
    if (!state_topic_.empty())
    {
        ros::SubscribeOptions ops = ros::SubscribeOptions::create<nav_msgs::Odometry>(
                    state_topic_, 1,
                    boost::bind(&NavdataPlugin::StateCallback, this, _1),
                    ros::VoidPtr(), &callback_queue_);
        state_subscriber_ = node_handle_->subscribe(ops);

        ROS_INFO_NAMED("quadrotor_state_controller", "Using state information on topic %s as source of state information.", state_topic_.c_str());
    }

    updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&NavdataPlugin::Update, this));

    take_off_subscriber_ = node_handle_->subscribe(takeoff_topic_,10, &NavdataPlugin::takeoffCB, this);
    land_subscriber_ = node_handle_->subscribe(land_topic_,10, &NavdataPlugin::land, this);
    first = false;


}

void NavdataPlugin::takeoffCB(std_msgs::Empty msg){
    if (robot_current_state == LANDED){
        pose = link->GetWorldPose();
        x_offset = pose.pos.x;
        y_offset = pose.pos.y;
        z_offset = pose.pos.z;

        geometry_msgs::Twist twist;
        ROS_INFO_STREAM("taking off, altitude: " << pose.pos.z);
        ros::Rate r(10);
        pose = link->GetWorldPose();
        while (pose.pos.z < 0.5){
            pose = link->GetWorldPose();
            link->SetLinearVel(math::Vector3(0, 0, 0.3));
            twist.linear.z = 0.3;
            cmd_velPub.publish(twist);
            r.sleep();
        }
        link->SetLinearVel(math::Vector3(0, 0, 0.3));
        cmd_velPub.publish(twist);

    }
}

void NavdataPlugin::land(std_msgs::Empty msg){
    if (robot_current_state == FLYING){
        geometry_msgs::Twist twist;
        ROS_INFO("landing");
        ros::Rate r(10);
        while (pose.pos.z > 0){
            pose = link->GetWorldPose();
            //twist.linear.z = 0.3;
            //cmd_velPub.publish(twist);
            r.sleep();
        }
        twist.linear.z = 0.0;
        cmd_velPub.publish(twist);
    }
}


void NavdataPlugin::ImuCallback(const sensor_msgs::ImuConstPtr& imu)
{
  imu_msg = imu;
  pose.rot.Set(imu->orientation.w, imu->orientation.x, imu->orientation.y, imu->orientation.z);
  euler = pose.rot.GetAsEuler();
  angular_velocity = pose.rot.RotateVector(math::Vector3(imu->angular_velocity.x, imu->angular_velocity.y, imu->angular_velocity.z));
}

void NavdataPlugin::SonarCallback(const sensor_msgs::RangeConstPtr& sonar_info)
{
  robot_altitude = sonar_info->range;
}


void NavdataPlugin::StateCallback(const nav_msgs::OdometryConstPtr& state)
{
  math::Vector3 velocity1(velocity);


  /*if (imu_topic_.empty()) {
    pose.pos.Set(state->pose.pose.position.x, state->pose.pose.position.y, state->pose.pose.position.z);
    pose.rot.Set(state->pose.pose.orientation.w, state->pose.pose.orientation.x, state->pose.pose.orientation.y, state->pose.pose.orientation.z);
    euler = pose.rot.GetAsEuler();
    angular_velocity.Set(state->twist.twist.angular.x, state->twist.twist.angular.y, state->twist.twist.angular.z);
  }*/

  velocity.Set(state->twist.twist.linear.x, state->twist.twist.linear.y, state->twist.twist.linear.z);

  // calculate acceleration
  double dt = !state_stamp.isZero() ? (state->header.stamp - state_stamp).toSec() : 0.0;
  state_stamp = state->header.stamp;
  if (dt > 0.0) {
    acceleration = (velocity - velocity1) / dt;
  } else {
    acceleration.Set();
  }
  
  if (state->pose.pose.position.z > 0.05){
	  	robot_current_state = FLYING;
  } else {
	  	robot_current_state = LANDED;
  }
  
  
}

float NavdataPlugin::RandomFloat(float min, float max)
{
    if(first){
        srand (time(NULL));
        first=true;
    }

    return ((float(rand()) / float(RAND_MAX)) * (max - min)) + min;

}

void NavdataPlugin::Reset()
{
  // reset state
  pose.Reset();
  velocity.Set();
  angular_velocity.Set();
  acceleration.Set();
  euler.Set();
  state_stamp = ros::Time();
}

void NavdataPlugin::Update()
{
  math::Vector3 force, torque;

  // Get new commands/state
  callback_queue_.callAvailable();

  // Get simulator time
  common::Time sim_time = world->GetSimTime();
  double dt = (sim_time - last_time).Double();
  // Update rate is 200/per second
  if (dt < 0.005) return;

  truth_pose = link->GetWorldPose();
  //angular_velocity = link->GetWorldAngularVel();
  //euler = pose.rot.GetAsEuler();
  //acceleration = (link->GetWorldLinearVel() - velocity) / dt;
  //velocity = link->GetWorldLinearVel();

  // Rotate vectors to coordinate frames relevant for control
  math::Quaternion heading_quaternion(cos(euler.z/2),0,0,sin(euler.z/2));
  math::Vector3 velocity_xy = heading_quaternion.RotateVectorReverse(velocity);
  math::Vector3 acceleration_xy = heading_quaternion.RotateVectorReverse(acceleration);
  math::Vector3 angular_velocity_body = pose.rot.RotateVectorReverse(angular_velocity);

  drone_msgs::Navdata navdata;
  if (robot_current_state == FLYING){
	navdata.vx = 1000*(velocity_xy.x + this->RandomFloat(-trans_drift, trans_drift));
	navdata.vy = 1000*(velocity_xy.y + this->RandomFloat(-trans_drift, trans_drift));
	navdata.vz = 1000*(velocity_xy.z + this->RandomFloat(-trans_drift, trans_drift));
  } 
  else {
	navdata.vx = 1000*(velocity_xy.x + this->RandomFloat(-0, 0));
	navdata.vy = 1000*(velocity_xy.y + this->RandomFloat(-0, 0));
	navdata.vz = 1000*(velocity_xy.z + this->RandomFloat(-0, 0));
  }

  navdata.ax = acceleration_xy.x/10;
  navdata.ay = acceleration_xy.y/10;
  navdata.az = acceleration_xy.z/10 + 1;

  
  navdata.batteryPercent = 100;
  navdata.rotX = (pose.rot.GetRoll()) * 180.0 / M_PI;
  navdata.rotY = (pose.rot.GetPitch()) * 180.0 / M_PI;
  navdata.rotZ = (pose.rot.GetYaw()) * 180.0 / M_PI;

  navdata.altd = (truth_pose.pos.z + this->RandomFloat(-alt_drift, alt_drift)) * 1000.f;


  navdata.tm = ros::Time::now().toSec()*1000000;

  navdata.header.stamp = ros::Time::now();
  navdata.header.frame_id = link_name_;
  navdata.state = 3;
  navdata.magX = 0;
  navdata.magY = 0;
  navdata.magZ = 0;
  navdata.pressure = 0;
  navdata.temp = 0;
  navdata.wind_speed = 0.0;
  navdata.wind_angle = 0.0;
  navdata.wind_comp_angle = 0.0;
  navdata.tags_count = 0;

  m_navdataPub.publish( navdata );

  mavros_msgs::RangeFinder rf;
  rf.header.stamp = ros::Time::now();
  rf.distance = truth_pose.pos.z - 0.10000000149;
  sonarPub.publish(rf);

  // save last time stamp
  last_time = sim_time;

}

////////////////////////////////////////////////////////////////////////////////
// Update the controller
void NavdataPlugin::UpdateChild()
{
}

}
